FROM node:boron

# Vars
ARG port=3000
ARG url
ARG private_key_path
ARG extra_rules

# Create app directory
RUN mkdir -p /usr/src/git-linter-service
WORKDIR /usr/src/git-linter-service

# Install app dependencies
COPY package.json /usr/src/git-linter-service/
RUN npm install
RUN npm install $extra_rules

# Bundle app source
COPY . /usr/src/git-linter-service/

# Copy private key in the docker
COPY $private_key_path /usr/src/git-linter-service/private/private-key.pem

ENV PORT=$port
ENV URL=$url
ENV PRIVATE_KEY_PATH="/usr/src/git-linter-service/private/private-key.pem"
ENV PLUGINS=$extra_rules

EXPOSE $port

CMD [ "npm", "start" ]
