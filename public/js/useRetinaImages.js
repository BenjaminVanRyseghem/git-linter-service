(() => {
    if (window.devicePixelRatio > 1) {
        jQuery(() => {
            jQuery("img").each((index, node) => {
                let newSrc = node.src.replace(/\.png$/, "@2x.png");
                node.src = newSrc;
            });
        });
    }
})();
