(function() {
    let locations = jQuery(".anomaly-location");
    locations.each(function(index, location) {
        let uuid = location.getAttribute("data-uuid");
        let mark = jQuery(`[data-error-uuid=${uuid}]`);

        jQuery(location).hover(function() {
            mark.addClass("highlighted");
        }, function() {
            mark.removeClass("highlighted");
        });
    });
})();
