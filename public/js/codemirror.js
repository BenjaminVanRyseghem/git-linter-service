window.initializeCodeMirror = function(data) {
    // var data = JSON.parse(json);
    var message = data.message;
    var errors = data.errors;
    var warnings = data.warnings;

    var myCodeMirror = CodeMirror(document.getElementById("message"), {
        value: message,
        lineNumbers: true,
        readOnly: true,
        rulers: [
            {
                column: 50,
                className: "short-description-limit"
            },
            {
                column: 72,
                className: "long-description-limit"
            }
        ],
        fixedGutter: false
    });

    errors.forEach(function(error, index) {
        var node = document.createElement("div");
        node.className = "marker error";
        node.setAttribute("data-error-uuid", error.uuid);
        node.setAttribute("title", error.errorOptions.message);

        myCodeMirror.addWidget({
                line: error.errorOptions.loc.line - 1,
                ch: error.errorOptions.loc.column
            },
            node,
            true
        );
    });

    warnings.forEach(function(error, index) {
        var node = document.createElement("div");
        node.className = "marker warning";
        node.setAttribute("data-error-uuid", error.uuid);
        node.setAttribute("title", error.errorOptions.message);

        myCodeMirror.addWidget({
                line: error.errorOptions.loc.line - 1,
                ch: error.errorOptions.loc.column
            },
            node,
            true
        );
    });
};
