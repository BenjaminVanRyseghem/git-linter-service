let types = {};

types.any = function() {
    return true;
};

types.null = function(value) {
    return value === "null";
};

types.boolean = function(value) {
    return value === "true" || value === "false";
};

types.number = function(value) {
    if (value === "NaN") {
        return true;
    }

    value = +value;
    return !isNaN(value) && typeof value === "number";
};

types.integer = function(value) {
    if (value === "NaN") {
        return true;
    }

    value = +value;
    return !isNaN(value) && typeof value === "number" && (Math.floor(value) === value || value > 9007199254740992 || value < -9007199254740992);
};

types.spaceSeparatedStrings = function(value) {
    return true;
};

types.string = function(value) {
    return true;
};

module.exports = {
    hasCheckerFor: (type) => !!types[type],
    check: (value, type) => {
        let checker = types[type];

        if (!checker) {
            return null;
        }

        return checker(value);
    }
};
