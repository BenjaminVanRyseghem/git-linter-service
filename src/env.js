const spec = {
    "PORT": {
        mandatory: false,
        default: 3000,
        type: "integer"
    },
    "URL": {
        mandatory: true,
        type: "string"
    },
    "PRIVATE_KEY_PATH": {
        default: `${__dirname}/../private/git-lint-ci.private-key.pem`,
        type: "string"
    },
    "PRIVATE_KEY": {
        type: "string"
    },
    "PLUGINS": {
        type: "spaceSeparatedStrings",
        default: []
    }
};

const defaultType = "string";

function warn(name, message) {
    console.warn(`WARN: ${message} [${name}]`); // eslint-disable-line no-console
}

function error(name, message) {
    console.error(`ERR: ${message} [${name}]`); // eslint-disable-line no-console
}

function getEnvValueFor(name) {
    return process.env[name];
}

/**
 * Return `true` when the whole SPEC is valid. Return `false` otherwise.
 */
function validateSpec(spec, checkers) {
    return allEntries(spec, checkers, validateSpecEntry);
}

/**
 * Return `true` when ENTRY is valid. Return `false` otherwise.
 */
function validateSpecEntry(entry, name, checkers) {
    let isMandatory = entry.mandatory;
    let type = entry.type;

    if (entry.hasOwnProperty("default") && isMandatory) {
        warn(name, "No need of \"default\" for mandatory entries");
    }

    if (!checkers.hasCheckerFor(type)) {
        error(name, `Invalid type "${type}"`);
        return false;
    }

    return true;
}

/**
 * Return `true` when SPEC is valid. Return `false` otherwise.
 */
function validateEnvValues(spec, checkers) {
    return allEntries(spec, checkers, validateSpecValue);
}

/**
 * Return `true` when the env variable NAME matches the specification SPEC.
 */
function validateSpecValue(entry, name, checkers) {
    let isMandatory = entry.mandatory;
    let type = entry.type || defaultType;

    let value = getEnvValueFor(name);

    if (!value) {
        if (isMandatory) {
            error(name, "mandatory field");
            return false;
        }
        return true;
    }

    let valid = checkers.check(value, type);

    if (!valid) {
        error(name, "mismatch the provided specification");
    }

    return valid;
}

function allEntries(spec, checkers, callback) {
    let valid = true;
    Object.keys(spec).forEach((name) => {
        let entry = spec[name];
        let result = callback(entry, name, checkers);
        valid = valid && result;
    });

    return valid;
}

function parseEnv(value, type) {
    if (value === undefined) {
        return undefined;
    }

    switch (type) {
        case "boolean": {
            if (value === "true") {
                return true;
            }
            if (value === "false") {
                return false;
            }
            throw new Error(`Cannot parse "${value}" as boolean`);
        }
        case "spaceSeparatedStrings": {
            return value.split(" ");
        }
        case "integer":
        case "number": {
            let result = +value;
            if (isNaN(result)) {
                throw new Error(`Cannot parse "${value}" as ${type}`);
            }
            return result;
        }
        default:
            if (value === "null") {
                return null;
            }
            if (value === "undefined") {
                return undefined;
            }
            return value;
    }
}

function getEnvValue(name, entry) {
    let parsedValue = parseEnv(getEnvValueFor(name), entry.type);

    if (parsedValue === undefined) {
        return entry.default;
    }

    return parsedValue;
}

function getEnvValues(spec) {
    let result = {};

    Object.keys(spec).forEach((name) => {
        let entry = spec[name];
        result[name] = getEnvValue(name, entry);
    });

    return result;
}

module.exports = () => {
    let checkers = require("./typeCheckers");
    let valid = validateSpec(spec, checkers);

    if (!valid) {
        return null;
    }

    valid = validateEnvValues(spec, checkers);

    if (!valid) {
        return null;
    }

    return getEnvValues(spec);
};
