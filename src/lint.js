const request = require("request");
const gitlint = require("git-linter");
const jsoncomp = require("jsoncomp");
const lz = require("lz-string");

let githubRequestQueue = [];
let gitlabRequestQueue = [];

let plugins = loadPlugins();

function lint(commit, options) {
    Object.assign(options.plugins || {}, plugins);
    return gitlint(commit, options);
}

function buildFailureDescription(lintResult) {
    let result = "Failing rules:\n";

    let rules = new Set(lintResult.getData().map((datum) => {
        return datum.rule;
    }));

    for (let rule of rules) {
        result += `- ${rule}`;
    }

    return result;
}

function buildTargetUrl(commit, lintResult) {
    const {URL, PORT} = require("./env")();
    let data = {
        sha: commit.shortSha(),
        message: commit.message(),
        errors: lintResult.getData().map((data) => {
            return {
                level: data.level,
                errorOptions: data.errorOptions,
                rule: data.rule
            };
        })
    };

    let json = lz.compress(JSON.stringify(jsoncomp.compress(data)));
    return `${URL}:${PORT}/info/${encodeURI(json)}`;
}

function markPending(commit, url, token) {
    markCommit("pending", "git linting in progress", "", commit, url, token);
}

function markError(errorMessage, commit, url, token) {
    markCommit("error", errorMessage, "", commit, url, token);
}

function markInvalidConf(errorMessage, name, commit, url, token) {
    const {URL, PORT} = require("./env")();
    markCommit("error", errorMessage, `${URL}:${PORT}/error/git-linter/invalidConfiguration/${encodeURIComponent(name)}`, commit, url, token);
}

function markSuccess(commit, url, targetUrl, token) {
    // Todo: add warnings to description
    markCommit("success", "git linting succeed", targetUrl, commit, url, token);
}

function markFailure(description, commit, targetUrl, url, token) {
    markCommit("failure", description, targetUrl, commit, url, token);
}

function markCommit(status, description, targetUrl, commit, url, token) {
    let type = commit.type();
    switch (type) {
        case "github":
            markGithubCommit(status, description, targetUrl, commit, url, token);
            break;
        case "gitlab":
            markGitlabCommit(status, description, targetUrl, commit, url, token);
            break;
        default:
            throw new Error(`Unsupported commit type "${type}"`);
    }
}

function markGithubCommit(status, description, targetUrl, commit, url, token) {
    let sha = commit.sha();

    function fn() {
        request({
            method: "POST",
            url: url.replace("{sha}", sha),
            headers: {
                "Authorization": `token ${token}`,
                "User-Agent": "git-linter-service",
                "Accept": "application/vnd.github.machine-man-preview+json"
            },
            json: true,
            body: {
                state: status,
                target_url: targetUrl,
                description: description,
                context: "git-linter"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 201) {
                console.error("Failed to create commit status:", body); // eslint-disable-line no-console
            }

            githubRequestQueue.shift(); // Remove head
            if (githubRequestQueue.length) {
                githubRequestQueue[0]();
            }
        });
    }

    githubRequestQueue.push(fn);
    if (githubRequestQueue.length === 1) {
        githubRequestQueue[0]();
    }
}

function markGitlabCommit(status, description, targetUrl, commit, url, token) {
    const {URL, PORT} = require("./env")();
    let sha = commit.sha();
    let gitlabStatus = convertToGitlabStatus(status);

    if (targetUrl.length > 255) {
        targetUrl = `${URL}:${PORT}/error/gitlab/target_url_too_long`;
    }

    function fn() {
        request({
            method: "POST",
            url: url.replace("{sha}", sha),
            headers: {
                "PRIVATE-TOKEN": token,
                "User-Agent": "git-linter-service"
            },
            json: true,
            body: {
                state: gitlabStatus,
                target_url: targetUrl,
                description: description,
                context: "git-linter"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 201) {
                console.error("Failed to create commit status:", body); // eslint-disable-line no-console
            }

            gitlabRequestQueue.shift(); // Remove head
            if (gitlabRequestQueue.length) {
                gitlabRequestQueue[0]();
            }
        });
    }

    gitlabRequestQueue.push(fn);
    if (gitlabRequestQueue.length === 1) {
        gitlabRequestQueue[0]();
    }
}

function convertToGitlabStatus(status) {
    switch (status) {
        case "pending":
            return "running";
        case "success":
            return "success";
        case "failure":
            return "failed";
        case "error":
            return "failed";
        default:
            throw new Error(`Unsupported commit status "${status}"`);
    }
}

function lintCommit(commit, options, url, token) {
    markPending(commit, url, token);
    let lintResult;

    try {
        lintResult = lint(commit, options);
    } catch (e) {
        markError(e.message, commit, url, token);
        return {
            success: false,
            result: null
        };
    }

    let targetUrl = buildTargetUrl(commit, lintResult);

    if (lintResult.isErrored()) {
        let description = buildFailureDescription(lintResult);
        markFailure(description, commit, targetUrl, url, token);
        return {
            success: false,
            result: lintResult
        };
    } else {
        markSuccess(commit, url, targetUrl, token);
        return {
            success: true,
            result: lintResult
        };
    }
}

function markLastCommit(head_commit, failingCommit, lintResult, statuses_url, token) {
    let targetUrl = buildTargetUrl(failingCommit.message(), lintResult);
    let sha = failingCommit.sha();
    sha = sha.slice(0, 8);
    markFailure(`commit ${sha} failed when linted`, head_commit, targetUrl, statuses_url, token);
}

function loadPlugins() {
    const {PLUGINS} = require("./env")();

    let result = {
        config: {},
        rules: {}
    };

    for (let name of PLUGINS) {
        // Plugin name should match a npm package name
        let rules = {};
        try {
            rules = require(name);
        } catch (error) {
            console.error(`Fail to load ${name}`); // eslint-disable-line no-console
            return process.exit(1);
        }

        if (rules.config) {
            for (let name of Object.keys(rules.config)) {
                let opt = rules.config[name];
                if (result.config[name]) {
                    console.error(`${name} already loaded`); // eslint-disable-line no-console
                } else {
                    result.config[name] = opt;
                }
            }
        }

        if (rules.rules) {
            for (let name of Object.keys(rules.rules)) {
                let opt = rules.rules[name];
                if (result.rules[name]) {
                    console.error(`${name} already loaded`); // eslint-disable-line no-console
                } else {
                    result.rules[name] = opt;
                }
            }
        }
    }

    return result;
}

module.exports = {
    markLastCommit,
    lintCommit,
    markInvalidConf
};
