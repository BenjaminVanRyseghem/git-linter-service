const fs = require("fs");
const path = require("path");

module.exports = function() {
    const env = require("./env")();

    let pathToPem = env.PRIVATE_KEY_PATH;
    let pem = env.PRIVATE_KEY;

    if (pem) {
        return pem;
    }

    return fs.readFileSync(path.resolve(pathToPem)).toString();
};
