const defaultConfig = {
    rules: {
        "short-description-length": 2,
        "long-description-length": 2,
        "empty-line": 2
    }
};

function extractConfigFromJson(string) {
    return JSON.parse(string);
}

function extractConfigFromJs(string) {
    const jailed = require("jailed");

    let module = "var module = {};\n";
    module += "try {\n";
    module += string;
    module += `; } catch(e) { module.exports = undefined; }
    var api = {
        result: function(fn) {
            fn(module.exports);
        }
    };  
    application.setInterface(api);`;

    let plugin = new jailed.DynamicPlugin(module);

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject("Timeout");
        }, 60000); // 1 minute timeout

        plugin.whenConnected(() => {
            plugin.remote.result(resolve);
        });

        plugin.whenFailed(() => {
            reject("Connection failed");
        });
    });
}

module.exports = (content, name) => {
    const path = require("path");
    return new Promise((resolve, reject) => {
        if (path.extname(name) === ".js") {
            extractConfigFromJs(content).then((data) => {
                resolve(data);
            }, () => {
                reject({
                    errorMessage: `Failed to collect the config file. Most probably the syntax is incorrect (${name})`,
                    configName: name
                });
            });
            return;
        } else if (path.extname(name) === ".json") {
            let config = extractConfigFromJson(content);
            resolve(config);
            return;
        } else {
            reject(new Error(`Unsupported file format ${path.extname(name)}`));
        }

        resolve(defaultConfig);
    });
};

module.exports.defaultConfig = Object.assign({}, defaultConfig);
