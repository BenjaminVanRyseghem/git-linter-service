const rewire = require("rewire");
const env = rewire("../src/env");

describe("env", () => {
    let revert;
    let consoleSpy;
    let processSpy;

    beforeEach(() => {
        consoleSpy = jasmine.createSpyObj("console", ["error", "warn", "log"]);
        processSpy = jasmine.createSpyObj("process", ["exit"]);
        processSpy.env = {};

        revert = env.__set__({
            "console": consoleSpy,
            "process": processSpy
        });
    });

    afterEach(() => {
        revert();
    });

    it("warns in the console", () => {
        let warn = env.__get__("warn");
        warn("foo", "boo");

        expect(consoleSpy.warn).toHaveBeenCalled();
    });

    it("errors in the console", () => {
        let error = env.__get__("error");
        error("foo", "boo");

        expect(consoleSpy.error).toHaveBeenCalled();
    });

    describe("getEnvValueFor", () => {
        it("get env variables from process.env", () => {
            let value = jasmine.createSpy("value");
            processSpy.env.foo = value;

            let getEnvValueFor = env.__get__("getEnvValueFor");

            expect(getEnvValueFor("foo")).toBe(value);
        });
    });

    describe("validateSpecValue", () => {
        let validateSpecValue;

        beforeEach(() => {
            validateSpecValue = env.__get__("validateSpecValue");
        });

        it("fetches the name-matching env variable", () => {
            let getEnvValueForSpy = jasmine.createSpy("getEnvValueFor");
            let revert = env.__set__("getEnvValueFor", getEnvValueForSpy);

            try {
                let entry = {
                    mandatory: true
                };

                let name = jasmine.createSpy("name");

                validateSpecValue(entry, name);
                expect(getEnvValueForSpy).toHaveBeenCalledWith(name);
                expect(getEnvValueForSpy).toHaveBeenCalledTimes(1);
            } finally {
                revert();
            }
        });

        it("is valid when the env does not have a matching variable is not found and the variable is optional", () => {
            let entry = {
                mandatory: false
            };

            let name = "SOMETHING_HOPEFULLY_NOT_IN_YOUR_ENV";
            processSpy.env = {};

            expect(validateSpecValue(entry, name)).toBeTruthy();
        });

        it("is not valid when the env does not have a matching variable is not found and the variable is mandatory", () => {
            let entry = {
                mandatory: true
            };

            let name = "SOMETHING_HOPEFULLY_NOT_IN_YOUR_ENV";
            processSpy.env = [];

            expect(validateSpecValue(entry, name)).toBeFalsy();
        });

        it("checks env variable type against the entry type", () => {
            let checkers = jasmine.createSpyObj("checkers", ["check"]);
            let name = jasmine.createSpy("name");
            let value = jasmine.createSpy("value");
            let type = jasmine.createSpy("entry type");

            let entry = {type};

            processSpy.env[name] = value;

            validateSpecValue(entry, name, checkers);

            expect(checkers.check).toHaveBeenCalledWith(value, type);
        });

        it("returns the check result", () => {
            let validateSpecValue = env.__get__("validateSpecValue");
            let name = jasmine.createSpy("name");
            let value = jasmine.createSpy("value");
            let type = jasmine.createSpy("entry type");
            let checkerResult = jasmine.createSpy("checkerResult");
            let checkers = jasmine.createSpyObj("checkers", ["check"]);
            checkers.check.and.returnValue(checkerResult);

            let entry = {type};

            processSpy.env[name] = value;

            let validationResult = validateSpecValue(entry, name, checkers);

            expect(validationResult).toBe(checkerResult);
        });
    });

    describe("validateSpecEntry", () => {
        let validateSpecEntry;
        let name;
        let entry;
        let checkers;

        beforeEach(() => {
            validateSpecEntry = env.__get__("validateSpecEntry");
            name = jasmine.createSpy("name");
            entry = jasmine.createSpy("entry");
            checkers = jasmine.createSpyObj("checkers", ["hasCheckerFor"]);
        });

        it("warns you when an entry is mandatory and has a default", () => {
            entry = {
                default: 4,
                mandatory: true
            };

            validateSpecEntry(entry, name, checkers);
            expect(consoleSpy.warn).toHaveBeenCalledWith(jasmine.stringMatching(/mandatory/i));
            expect(consoleSpy.warn).toHaveBeenCalledWith(jasmine.stringMatching(/default/i));
        });

        it("logs an error when the entry type is not supported", () => {
            checkers.hasCheckerFor.and.returnValue(false);

            validateSpecEntry(name, entry, checkers);
            expect(consoleSpy.error).toHaveBeenCalledWith(jasmine.stringMatching(/type/i));
            expect(consoleSpy.error).toHaveBeenCalledWith(jasmine.stringMatching(/invalid/i));
        });

        it("returns false when the entry type is not suppported", () => {
            checkers.hasCheckerFor.and.returnValue(false);

            let result = validateSpecEntry(name, entry, checkers);
            expect(result).toBeFalsy();
        });

        it("returns true when the entry type is matching a checker", () => {
            checkers.hasCheckerFor.and.returnValue(true);

            let result = validateSpecEntry(name, entry, checkers);
            expect(result).toBeTruthy();
        });
    });

    describe("validateSpec", () => {
        let localRevert;
        let validateSpec;
        let allEntriesSpy;

        beforeEach(() => {
            allEntriesSpy = jasmine.createSpy("allEntries");
            localRevert = env.__set__("allEntries", allEntriesSpy);
            validateSpec = env.__get__("validateSpec");
        });

        afterEach(() => {
            localRevert();
        });

        it("delegates to allEntries with validateSpecEntry", () => {
            let spec = jasmine.createSpy("spec");
            let checkers = jasmine.createSpy("checkers");
            let validateSpecEntry = env.__get__("validateSpecEntry");

            validateSpec(spec, checkers);
            expect(allEntriesSpy).toHaveBeenCalledWith(spec, checkers, validateSpecEntry);
        });
    });

    describe("validateEnvValues", () => {
        let localRevert;
        let validateEnvValues;
        let allEntriesSpy;

        beforeEach(() => {
            allEntriesSpy = jasmine.createSpy("allEntries");
            localRevert = env.__set__("allEntries", allEntriesSpy);
            validateEnvValues = env.__get__("validateEnvValues");
        });

        afterEach(() => {
            localRevert();
        });

        it("delegates to allEntries with validateSpecValue", () => {
            let spec = jasmine.createSpy("spec");
            let checkers = jasmine.createSpy("checkers");
            let validateSpecValue = env.__get__("validateSpecValue");

            validateEnvValues(spec, checkers);
            expect(allEntriesSpy).toHaveBeenCalledWith(spec, checkers, validateSpecValue);
        });
    });

    describe("allEntries", () => {
        let allEntries;
        let checkers;
        let callback;

        beforeEach(() => {
            allEntries = env.__get__("allEntries");
            checkers = jasmine.createSpy("checkers");
            callback = jasmine.createSpy("callback");
        });

        it("validates all the entry", () => {
            let spec = {a: 1, b: 2, c: 3};
            let numberOfEntries = Object.keys(spec).length;

            allEntries(spec, checkers, callback);
            expect(callback).toHaveBeenCalledTimes(numberOfEntries);
        });

        it("validates all the entry even when one fails", () => {
            callback.and.returnValues(false, false, true);

            let spec = {a: 1, b: 2, c: 3};
            let numberOfEntries = Object.keys(spec).length;

            allEntries(spec, checkers, callback);
            expect(callback).toHaveBeenCalledTimes(numberOfEntries);
        });

        it("returns false as long as one entry is invalid", () => {
            callback.and.returnValues(false, true, true);

            let spec = {a: 1, b: 2, c: 3};

            let valid = allEntries(spec, checkers, callback);
            expect(valid).toBeFalsy();
        });

        it("returns true if all entries are invalid", () => {
            callback.and.returnValues(true, true, true);

            let spec = {a: 1, b: 2, c: 3};

            let valid = allEntries(spec, checkers, callback);
            expect(valid).toBeTruthy();
        });
    });

    describe("parseEnv", () => {
        let parseEnv;

        beforeEach(() => {
            parseEnv = env.__get__("parseEnv");
        });

        it("parses `undefined` as `undefined`", () => {
            let result = parseEnv(undefined);

            expect(result).toBe(undefined);
        });

        it("parses `\"undefined\"` as `undefined`", () => {
            let result = parseEnv("undefined");

            expect(result).toBe(undefined);
        });

        it("parses `\"null\"` as `null`", () => {
            let result = parseEnv("null");

            expect(result).toBe(null);
        });

        it("parses `\"true\"` as `true`", () => {
            let result = parseEnv("true", "boolean");

            expect(result).toBe(true);
        });

        it("parses `\"false\"` as `false`", () => {
            let result = parseEnv("false", "boolean");

            expect(result).toBe(false);
        });

        it("throws an error for any other \"boolean\" values", () => {
            let fn = parseEnv.bind(null, "lars;ytl", "boolean");

            expect(fn).toThrowError();
        });

        it("throws an error when parsing a not number as number", () => {
            let fn = parseEnv.bind(null, "foo", "number");

            expect(fn).toThrowError();
        });

        it("throws an error when parsing a not integer as integer", () => {
            let fn = parseEnv.bind(null, "foo", "integer");

            expect(fn).toThrowError();
        });

        it("parses the number `\"123\"` as `123`", () => {
            let result = parseEnv("123", "number");

            expect(result).toBe(123);
        });

        it("parses the integer `\"123\"` as `123`", () => {
            let result = parseEnv("123", "integer");

            expect(result).toBe(123);
        });

        it("parses `\"foo bar\"` as `[\"foo\", \"bar\"]`", () => {
            let result = parseEnv("foo bar", "spaceSeparatedStrings");

            expect(result).toEqual(["foo", "bar"]);
        });

        it("let go through any other string", () => {
            let value = jasmine.createSpy("value");
            let result = parseEnv(value);

            expect(result).toBe(value);
        });
    });

    describe("getEnvValue", () => {
        let getEnvValue;
        let getEnvValueForSpy;
        let parseEnvSpy;
        let name;
        let revert;

        beforeEach(() => {
            getEnvValue = env.__get__("getEnvValue");
            name = jasmine.createSpy("name");
            getEnvValueForSpy = jasmine.createSpy("getEnvValueForSpy");
            parseEnvSpy = jasmine.createSpy("parseEnvSpy");
            revert = env.__set__({
                getEnvValueFor: getEnvValueForSpy,
                parseEnv: parseEnvSpy
            });
        });

        afterEach(() => {
            revert();
        });

        it("gets its value from the env", () => {
            let entry = {};

            getEnvValue(name, entry);

            expect(getEnvValueForSpy).toHaveBeenCalledWith(name);
        });

        it("parses the env variable", () => {
            let envVar = jasmine.createSpy("envVar");
            let type = jasmine.createSpy("entry type");
            getEnvValueForSpy.and.returnValue(envVar);

            let entry = {type};

            getEnvValue(name, entry);
            expect(parseEnvSpy).toHaveBeenCalledWith(envVar, jasmine.anything());
        });

        it("parses the env variable using the entry type", () => {
            let envVar = jasmine.createSpy("envVar");
            let type = jasmine.createSpy("entry type");
            getEnvValueForSpy.and.returnValue(envVar);

            let entry = {type};

            getEnvValue(name, entry);
            expect(parseEnvSpy).toHaveBeenCalledWith(jasmine.anything(), type);
        });

        it("returns the entry default if the value is undefined", () => {
            let defaultValue = jasmine.createSpy("entry default");
            parseEnvSpy.and.returnValue(undefined);

            let entry = {default: defaultValue};

            let result = getEnvValue(name, entry);
            expect(result).toBe(defaultValue);
        });

        it("returns the parsed value if not undefined", () => {
            let defaultValue = jasmine.createSpy("entry default");
            parseEnvSpy.and.returnValue(false);

            let entry = {default: defaultValue};

            let result = getEnvValue(name, entry);
            expect(result).not.toBe(defaultValue);
            expect(result).toBe(false);
        });
    });

    describe("getEnvValues", () => {
        let getEnvValues;
        let getEnvValueSpy;
        let revert;

        beforeEach(() => {
            getEnvValues = env.__get__("getEnvValues");
            getEnvValueSpy = jasmine.createSpy("getEnvValue");
            revert = env.__set__({
                getEnvValue: getEnvValueSpy
            });
        });

        afterEach(() => {
            revert();
        });

        it("calls getEnvValue for each variable", () => {
            let spec = {a: 1, b: 2, c: 3};
            let numberOfKeys = Object.keys(spec).length;

            getEnvValues(spec);
            expect(getEnvValueSpy).toHaveBeenCalledTimes(numberOfKeys);
        });

        it("calls getEnvValue with the name-matching entry", () => {
            let entry = jasmine.createSpy("entry");
            let spec = {a: entry};

            getEnvValues(spec);
            expect(getEnvValueSpy).toHaveBeenCalledWith("a", entry);
        });

        it("returns a map with a key for each variable", () => {
            let spec = {a: 1, b: 2, c: 3};
            let variables = Object.keys(spec);

            let result = getEnvValues(spec);
            expect(Object.keys(result)).toEqual(variables);
        });

        it("returns a map where each value is the env value matching the key", function() {
            let entry = jasmine.createSpy("entry");
            let valueForA = jasmine.createSpy("value");
            let spec = {a: entry};
            getEnvValueSpy.and.returnValue(valueForA);

            let result = getEnvValues(spec);
            expect(result.a).toEqual(valueForA);
        });
    });

    describe("main", () => {
        let validateSpecSpy;
        let validateEnvValuesSpy;
        let getEnvValuesSpy;
        let revert;

        beforeEach(() => {
            validateSpecSpy = jasmine.createSpy("validateSpec");
            validateEnvValuesSpy = jasmine.createSpy("validateSpec");
            getEnvValuesSpy = jasmine.createSpy("getEnvValues");
            revert = env.__set__({
                getEnvValues: getEnvValuesSpy,
                validateSpec: validateSpecSpy,
                validateEnvValues: validateEnvValuesSpy
            });
        });

        afterEach(() => {
            revert();
        });

        it("returns null if the spec is not valid", () => {
            validateSpecSpy.and.returnValue(false);
            let result = env();

            expect(result).toBe(null);
        });

        it("returns null if env does not match the spec", () => {
            validateSpecSpy.and.returnValue(true);
            validateEnvValuesSpy.and.returnValue(false);
            let result = env();

            expect(result).toBe(null);
        });

        it("returns the env values", () => {
            validateSpecSpy.and.returnValue(true);
            validateEnvValuesSpy.and.returnValue(true);

            let values = jasmine.createSpy("env values");
            getEnvValuesSpy.and.returnValue(values);

            let result = env();

            expect(result).toBe(values);
        });
    });
});
