const rewire = require("rewire");
const typeCheckers = rewire("../src/typeCheckers");

describe("env", () => {
    let types;

    beforeEach(() => {
        types = typeCheckers.__get__("types");
    });

    describe("any", () => {
        it("always return true", () => {
            expect(types.any("\"wfp\"")).toBe(true);
            expect(types.any("123")).toBe(true);
            expect(types.any("123.5")).toBe(true);
            expect(types.any("[2]")).toBe(true);
            expect(types.any("{a: 1}")).toBe(true);
            expect(types.any("true")).toBe(true);
            expect(types.any("false")).toBe(true);
            expect(types.any("null")).toBe(true);
            expect(types.any("undefined")).toBe(true);
        });
    });

    describe("spaceSeparatedStrings", () => {
        it("always return true II", () => {
            expect(types.spaceSeparatedStrings("\"wfp\"")).toBe(true);
            expect(types.spaceSeparatedStrings("123")).toBe(true);
            expect(types.spaceSeparatedStrings("123.5")).toBe(true);
            expect(types.spaceSeparatedStrings("[2]")).toBe(true);
            expect(types.spaceSeparatedStrings("{a: 1}")).toBe(true);
            expect(types.spaceSeparatedStrings("true")).toBe(true);
            expect(types.spaceSeparatedStrings("false")).toBe(true);
            expect(types.spaceSeparatedStrings("null")).toBe(true);
            expect(types.spaceSeparatedStrings("undefined")).toBe(true);
            expect(types.spaceSeparatedStrings("foo bar")).toBe(true);
        });
    });

    describe("null", () => {
        it("returns true when `null` is provided", () => {
            expect(types.null("null")).toBe(true);
        });

        it("returns false for everything but `null`", () => {
            expect(types.null("\"wfp\"")).toBe(false);
            expect(types.null("123")).toBe(false);
            expect(types.null("123.5")).toBe(false);
            expect(types.null("[2]")).toBe(false);
            expect(types.null("{a: 1}")).toBe(false);
            expect(types.null("true")).toBe(false);
            expect(types.null("false")).toBe(false);
            expect(types.null("undefined")).toBe(false);
        });
    });

    describe("boolean", () => {
        it("returns true when `true` is provided", () => {
            expect(types.boolean("true")).toBe(true);
        });

        it("returns true when `false` is provided", () => {
            expect(types.boolean("false")).toBe(true);
        });

        it("returns false for everything but a boolean", () => {
            expect(types.boolean("\"wfp\"")).toBe(false);
            expect(types.boolean("123")).toBe(false);
            expect(types.boolean("123.5")).toBe(false);
            expect(types.boolean("[2]")).toBe(false);
            expect(types.boolean("{a: 1}")).toBe(false);
            expect(types.boolean("null")).toBe(false);
            expect(types.boolean("undefined")).toBe(false);
        });
    });

    describe("number", () => {
        it("returns true when a number is provided", () => {
            expect(types.number("123")).toBe(true);
        });

        it("returns true when an integer is provided as number", () => {
            expect(types.number("123.5")).toBe(true);
        });

        it("considers \"NaN\" a number", () => {
            expect(types.number("NaN")).toBe(true);
        });

        it("returns false for everything but a number", () => {
            expect(types.number("\"wfp\"")).toBe(false);
            expect(types.number("[1]")).toBe(false);
            expect(types.number("{a: 1}")).toBe(false);
            expect(types.number("true")).toBe(false);
            expect(types.number("false")).toBe(false);
            expect(types.number("null")).toBe(false);
            expect(types.number("undefined")).toBe(false);
        });
    });

    describe("integer", () => {
        it("returns true when an integer is provided", () => {
            expect(types.integer("123")).toBe(true);
        });

        it("considers \"NaN\" an integer", () => {
            expect(types.integer("NaN")).toBe(true);
        });

        it("returns false for everything but an integer", () => {
            expect(types.integer("\"wfp\"")).toBe(false);
            expect(types.integer("123.5")).toBe(false);
            expect(types.integer("[1]")).toBe(false);
            expect(types.integer("{a: 1}")).toBe(false);
            expect(types.integer("true")).toBe(false);
            expect(types.integer("false")).toBe(false);
            expect(types.integer("null")).toBe(false);
            expect(types.integer("undefined")).toBe(false);
        });
    });

    describe("string", () => {
        it("returns true when an string is provided. Everything from ENV is a string", () => {
            expect(types.string("\"wfp\"")).toBe(true);
            expect(types.string("123")).toBe(true);
            expect(types.string("123.5")).toBe(true);
            expect(types.string("[1]")).toBe(true);
            expect(types.string("{a: 1}")).toBe(true);
            expect(types.string("true")).toBe(true);
            expect(types.string("false")).toBe(true);
            expect(types.string("null")).toBe(true);
            expect(types.string("undefined")).toBe(true);
        });
    });

    describe("hasCheckerFor", () => {
        it("returns true when the type matches a checker name", () => {
            expect(typeCheckers.hasCheckerFor("any")).toBe(true);
            expect(typeCheckers.hasCheckerFor("null")).toBe(true);
            expect(typeCheckers.hasCheckerFor("boolean")).toBe(true);
            expect(typeCheckers.hasCheckerFor("number")).toBe(true);
            expect(typeCheckers.hasCheckerFor("integer")).toBe(true);
            expect(typeCheckers.hasCheckerFor("string")).toBe(true);
        });

        it("returns false for every other type", () => {
            expect(typeCheckers.hasCheckerFor("not existing type")).toBe(false);
        });
    });

    describe("check", () => {
        it("returns null when no checker is found", () => {
            let value = jasmine.createSpy("value");
            expect(typeCheckers.check(value, "not existing type")).toBe(null);
        });

        it("the checker is called with the provided value", () => {
            let value = jasmine.createSpy("value");
            let checker = jasmine.createSpy("checker");
            types["new type"] = checker;
            let checkerResult = jasmine.createSpy("checkerResult");
            checker.and.returnValue(checkerResult);

            typeCheckers.check(value, "new type");
            expect(checker).toHaveBeenCalledWith(value);
        });

        it("returns the checker value when found", () => {
            let value = jasmine.createSpy("value");
            let checker = jasmine.createSpy("checker");
            types["new type"] = checker;
            let checkerResult = jasmine.createSpy("checkerResult");
            checker.and.returnValue(checkerResult);

            let result = typeCheckers.check(value, "new type");
            expect(result).toBe(checkerResult);
        });
    });
});
