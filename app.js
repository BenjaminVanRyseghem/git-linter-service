const express = require("express");
const session = require("express-session");
const path = require("path");
// const favicon = require("serve-favicon");
const bodyParser = require("body-parser");

const routesInitializer = require("./routes/index");

function initialize() {
    const app = express();
    let routes = routesInitializer();

    let env = process.env.NODE_ENV || "development";
    app.locals.ENV = env;
    app.locals.ENV_DEVELOPMENT = env === "development";

    let sess = {
        secret: "keyboard cat",
        resave: false,
        saveUninitialized: true
    };

    if (app.get("env") === "production") {
        app.set("trust proxy", 1); // trust first proxy
        sess.cookie.secure = true; // serve secure cookies
    }

    app.use(session(sess));

    // view engine setup

    app.set("views", path.join(__dirname, "views"));
    app.set("view engine", "pug");

    // app.use(favicon(__dirname + '/public/img/favicon.ico'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use("/public", express.static(path.join(__dirname, "public")));

    app.use("/", routes);

    /// catch 404 and forward to error handler
    app.use(function(req, res, next) {
        let err = new Error("Not Found");
        err.status = 404;
        next(err);
    });

    /// error handlers

    // development error handler
    // will print stacktrace

    if (app.get("env") === "development") {
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render("error", {
                message: err.message,
                error: err,
                title: "error"
            });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render("error", {
            message: err.message,
            error: {},
            title: "error"
        });
    });

    return app;
}

module.exports = initialize;
