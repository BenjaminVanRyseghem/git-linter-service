let gulp = require("gulp");
let reporters = require("jasmine-reporters");

let plugins = require("gulp-load-plugins")({
    rename: {
        "gulp-eslint": "eslint",
        "gulp-istanbul": "istanbul",
        "gulp-jasmine": "jasmine",
        "gulp-plumber": "plumber",
        "gulp-sass": "sass"
    }
});

let sources = ["./bin/www", "./src/**/*.js", "./routes/**/*.js", "./public/js/**/*.js"];
let tests = ["./tests/**/*Spec.js"];
let misc = ["./gulpfile.js", "./.eslintrc.js"];
let all = sources.slice().concat(tests).concat(misc);

gulp.task("lint", ["lint:js"]);
gulp.task("build", ["css"]);
gulp.task("default", ["lint", "test"]);
gulp.task("check", ["lint", "test-without-coverage"]);

//
// Lint
//

gulp.task("lint:js", () => {
    var pipeline = gulp.src(all)
        .pipe(plugins.eslint())
        .pipe(plugins.eslint.format("unix"))
        .pipe(plugins.eslint.failAfterError());

    return pipeline;
});

//
// Tests
//

gulp.task("test:setup", () => {
    return gulp.src(sources)
        .pipe(plugins.istanbul())
        .pipe(plugins.istanbul.hookRequire());
});

gulp.task("test", ["test:setup"], () => {
    return gulp.src(tests)
        .pipe(plugins.jasmine({
            reporter: new reporters.TerminalReporter()
        }))
        .pipe(plugins.istanbul.writeReports());
    // .pipe(plugins.istanbul.enforceThresholds({thresholds: {global: 90}}));
});

gulp.task("test-without-coverage", () => {
    return gulp.src(tests)
        .pipe(plugins.jasmine());
});

//
// Css
//

gulp.task("css", function() {
    return gulp.src("./style/*.scss")
        .pipe(plugins.plumber())
        .pipe(plugins.sass())
        // add css minification/concatenation task
        .pipe(gulp.dest("./public/css/"));
});

gulp.task("watch", function() {
    return gulp.watch("./style/*.scss", ["css"]);
});
