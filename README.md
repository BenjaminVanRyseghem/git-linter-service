# git-linter-service-service

[![build status](https://gitlab.com/BenjaminVanRyseghem/git-linter-service/badges/master/build.svg)](https://gitlab.com/BenjaminVanRyseghem/git-linter-service/commits/master) [![coverage report](https://gitlab.com/BenjaminVanRyseghem/git-linter-service/badges/master/coverage.svg)](https://gitlab.com/BenjaminVanRyseghem/git-linter-service/commits/master)

A Docker-based GitHub integration using [git-lint](https://gitlab.com/BenjaminVanRyseghem/git-lint) to check
git commits validity.

# Where to get Docker image

# How to run the image
## Env vars

# How to build my own to add my own rules (NOT YET SUPPORTED)
