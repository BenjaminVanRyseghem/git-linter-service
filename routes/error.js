const gitlab = require("./error/gitlab");
const gitLinter = require("./error/git-linter");

module.exports = {
    installRoutes: (prefix, router) => {
        gitlab.installRoutes(`${prefix}/error`, router);
        gitLinter.installRoutes(`${prefix}/error`, router);
    }
};
