const express = require("express");

const hooks = require("./hooks");
const info = require("./info");
const error = require("./error");

function initialize() {
    const router = express.Router();

    hooks.installRoutes("", router);
    info.installRoutes("", router);
    error.installRoutes("", router);

    /* GET home page. */

    router.get("/", function(req, res) {
        res.render("index", {
            title: "git-linter"
        });
    });

    return router;
}

module.exports = initialize;
