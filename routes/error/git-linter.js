const invalidConfiguration = require("./git-linter/invalidConfiguration");

module.exports = {
    installRoutes: (prefix, router) => {
        invalidConfiguration.installRoutes(`${prefix}/git-linter`, router);
    }
};
