function installRoutes(prefix, router) {
    router.get(`${prefix}/invalidConfiguration/:name`, function(req, res) {
        let {params: {name}} = req;
        res.render("error/git-linter/invalidConfiguration", {
            title: "Error: invalidConfiguration",
            name: decodeURI(name)
        });
    });
}

module.exports = {installRoutes};
