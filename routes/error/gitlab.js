const target_url_too_long = require("./gitlab/target_url_too_long");

module.exports = {
    installRoutes: (prefix, router) => {
        target_url_too_long.installRoutes(`${prefix}/gitlab`, router);
    }
};
