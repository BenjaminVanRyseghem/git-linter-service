function installRoutes(prefix, router) {
    router.get(`${prefix}/target_url_too_long`, function(req, res) {
        res.render("error/gitlab/target_url_too_long", {
            title: "Error: target_url_too_long"
        });
    });
}

module.exports = {installRoutes};
