const github = require("./hooks/github");
const gitlab = require("./hooks/gitlab");

module.exports = {
    installRoutes: (prefix, router) => {
        github.installRoutes(`${prefix}/hooks`, router);
        gitlab.installRoutes(`${prefix}/hooks`, router);
    }
};
