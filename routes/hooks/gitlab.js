const request = require("request");
const fs = require("fs");
const Promise = require("promise");
const confReader = require("../../src/confReader");

const lint = require("../../src/lint");

function installRoutes(prefix, router) {
    router.post(`${prefix}/gitlab`, function(req, res) {

        // Todo: read from env
        let secretToken = "tokentoken"; // to read from env`

        let {headers: {"x-gitlab-token": providedToken, "x-gitlab-event": eventType}} = req;

        if (secretToken !== providedToken) {
            res.sendStatus(404).end();
            return;
        }

        res.sendStatus(200).end();

        if (eventType === "Push Hook") {
            handlePush(req);
        }
        if (eventType === "Merge Request Hook") {
            let action = req.body.object_attributes.action;
            if (action !== "close") {
                handleMergeRequest(req);
            }
        }
    });
}

function handlePush(req) {
    let {body: {commits, "project_id": projectId}} = req;

    getToken().then((token) => {
        commits.forEach((commit) => {
            return getFiles(commit, projectId, token).then((files) => {
                let lintCommit = require("git-linter").commit({
                    sha: commit.id,
                    type: "gitlab",
                    message: commit.message
                });
                getRules(commit, projectId, token).then((options) => {
                    lint.lintCommit(lintCommit, options, `https://gitlab.com/api/v4/projects/${projectId}/statuses/{sha}`, token);
                }, ({errorMessage, configName}) => {
                    lint.markInvalidConf(errorMessage, configName, lintCommit, `https://gitlab.com/api/v4/projects/${projectId}/statuses/{sha}`, token);
                });
            });
        });
    });
}

function handleMergeRequest(req) {
    let {body: {object_attributes: {last_commit, iid, "target_project_id": projectId}}} = req;

    getToken().then((token) => {
        getMergeRequestCommits(iid, projectId, token).then((commits) => {
            return Promise.all(commits.map((commit) => {
                return getFiles(commit, projectId, token).then((files) => {
                    let lintCommit = require("git-linter").commit({
                        sha: commit.id,
                        type: "gitlab",
                        message: commit.message,
                        files: files
                    });
                    return getRules(commit, projectId, token).then((options) => {
                        let result = lint.lintCommit(lintCommit, options, `https://gitlab.com/api/v4/projects/${projectId}/statuses/{sha}`, token);
                        return {
                            success: result.success,
                            result: result.result,
                            commit: lintCommit
                        };
                    }, ({errorMessage, configName}) => {
                        lint.markInvalidConf(errorMessage, configName, lintCommit, `https://gitlab.com/api/v4/projects/${projectId}/statuses/{sha}`, token);
                    });
                });
            })).then((results) => {
                let hasFailed = results.find((result) => !result.success);
                if (hasFailed && commits.length > 1) {
                    let headCommit = require("git-linter").commit({
                        sha: last_commit.id,
                        type: "gitlab",
                        message: last_commit.message
                    });
                    lint.markLastCommit(headCommit, hasFailed.commit, hasFailed.result, `https://gitlab.com/api/v4/projects/${projectId}/statuses/{sha}`, token);
                }
            });
        });
    });
}

function getToken() {
    return new Promise((resolve, reject) => {
        // Todo use env
        fs.readFile("/Users/benjamin/projects/git-linter-service/private/gitlab-personal-access-token", (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data.toString());
        });
    });
}

function getRules(commit, id, token) {
    let candidates = [".gitlinterrc-service.js", ".gitlinterrc-service.json", ".gitlinterrc.js", ".gitlinterrc.json"];
    return Promise.all(candidates.map((name) => {
        return new Promise((resolve, reject) => {
            let url = `https://gitlab.com/api/v4/projects/${id}/repository/files/${encodeURIComponent(name)}?ref=${commit.id}`;
            request({
                method: "GET",
                url: url,
                headers: {
                    "PRIVATE-TOKEN": token,
                    "User-Agent": "git-linter-service"
                }
            }, (error, response, body) => {
                if (!response || (response.statusCode !== 200 && response.statusCode !== 404)) {
                    console.error("Failed to fetch rules:", body); // eslint-disable-line no-console
                    reject(response);
                    return;
                }

                if (response.statusCode === 404) {
                    resolve(undefined);
                    return;
                }

                resolve(JSON.parse(body));
            });
        });
    })).then((candidates) => {
        let file = candidates.find((candidate) => {
            return candidate !== undefined;
        });

        if (!file) {
            return new Promise((resolve) => {
                resolve(confReader.defaultConfig);
            });
        }

        return confReader(new Buffer(file.content, "base64").toString("ascii"), file.file_name);
    });
}

function getMergeRequestCommits(mergeRequestId, projectId, token) {
    return new Promise((resolve, reject) => {
        let url = `https://gitlab.com/api/v4/projects/${projectId}/merge_requests/${mergeRequestId}/commits`;
        request({
            method: "GET",
            url: url,
            headers: {
                "PRIVATE-TOKEN": token,
                "User-Agent": "git-linter-service"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 200) {
                console.error("Failed to fetch merge request commits:", body); // eslint-disable-line no-console
                reject(response);
                return;
            }

            resolve(JSON.parse(body));
        });
    });
}

function getFiles(commit, projectId, token) {
    const file = require("git-linter").file;
    return new Promise((resolve, reject) => {
        let url = `https://gitlab.com/api/v4/projects/${projectId}/repository/commits/${commit.id}/diff`;
        request({
            method: "GET",
            url: url,
            headers: {
                "PRIVATE-TOKEN": token,
                "User-Agent": "git-linter-service"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 200) {
                console.error("Failed to fetch files:", body); // eslint-disable-line no-console
                reject(response);
                return;
            }

            let diffData = JSON.parse(body);
            resolve(diffData.map((datum) => {
                return file({
                    path: datum.new_path,
                    mode: datum.b_mode
                });
            }));
        });
    });
}

module.exports = {installRoutes};
