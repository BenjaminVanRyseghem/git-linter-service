const request = require("request");
const jwt = require("jsonwebtoken");
const Promise = require("promise");

const lint = require("../../src/lint");
const confReader = require("../../src/confReader");
const integrationPemGetter = require("../../src/integrationPem");
const CryptoJS = require("crypto-js");

// Todo: read from env
const integration_id = 1518;
let integrationPem;

function installRoutes(prefix, router) {
    integrationPem = integrationPemGetter();
    router.post(`${prefix}/github`, function(req, res) {
        let {headers: {"x-github-event": eventType}} = req;

        // Todo: read from env
        let secretToken = "tokentoken"; // to read from env`

        if (!checkSignature(req, secretToken)) {
            res.sendStatus(404).end();
            return;
        }

        res.sendStatus(200).end();

        if (eventType === "push") {
            handlePush(req);
        }

        if (eventType === "pull_request") {
            handlePullRequest(req);
        }
    });
}

function checkSignature(req, secretToken) {
    let {headers: {"x-hub-signature": providedToken}, body} = req;

    let signature = `sha1=${CryptoJS.HmacSHA1(JSON.stringify(body), secretToken)}`;
    return signature === providedToken;
}

function handlePush(req) {
    let {body: {installation, commits, repository: {trees_url, statuses_url}}} = req;

    getToken(installation).then((token) => {
        commits.forEach((commit) => {
            return getFiles(commit, token).then((files) => {
                getRules(commit, trees_url, token).then((options) => {
                    let lintCommit = require("git-linter").commit({
                        sha: commit.sha,
                        message: commit.message,
                        type: "github",
                        files: files
                    });
                    lint.lintCommit(lintCommit, options, statuses_url, token);
                });
            });
        });
    });
}

function handlePullRequest(req) {
    let {body: {installation, pull_request: {head, base}, repository: {statuses_url, compare_url, trees_url}}} = req;
    getToken(installation).then((token) => {
        getComparisonBetween(base, head, compare_url, token).then((commits) => {
            return Promise.all(commits.map((commit) => {
                return getFiles(commit.commit, token).then((files) => {
                    return getRules(commit, trees_url, token).then((options) => {
                        let lintCommit = require("git-linter").commit({
                            sha: commit.sha,
                            message: commit.commit.message,
                            type: "github",
                            files: files
                        });
                        let result = lint.lintCommit(lintCommit, options, statuses_url, token);
                        return {
                            success: result.success,
                            result: result.result,
                            commit: lintCommit
                        };
                    });
                });
            })).then((results) => {
                let hasFailed = results.find((result) => !result.success);
                if (hasFailed && commits.length > 1) {
                    let headCommit = require("git-linter").commit(head);
                    lint.markLastCommit(headCommit, hasFailed.commit, hasFailed.result, statuses_url, token);
                }
            });
        });
    });
}

function getFiles(commit, token) {
    const file = require("git-linter").file;
    return new Promise((resolve, reject) => {
        request({
            method: "GET",
            url: commit.tree.url,
            headers: {
                "Authorization": `Bearer ${token}`,
                "User-Agent": "git-linter-service",
                "Accept": "application/vnd.github.machine-man-preview+json"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 200) {
                console.error("Failed to fetch files:", body); // eslint-disable-line no-console
                reject(response);
                return;
            }

            let rawFiles = JSON.parse(body).tree;
            resolve(rawFiles.map(file));
        });
    });
}

function getRules(commit, url, token) {
    return new Promise((resolve, reject) => {
        let finalUrl = url.replace("{/sha}", `/${commit.sha}`);
        request({
            method: "GET",
            url: finalUrl,
            headers: {
                "Authorization": `Bearer ${token}`,
                "User-Agent": "git-linter-service",
                "Accept": "application/vnd.github.machine-man-preview+json"
            }
        }, (error, response, body) => {
            if (!response || (response.statusCode !== 200 && response.statusCode !== 404)) {
                console.error("Failed fetch rules:", body); // eslint-disable-line no-console
                reject(response);
                return;
            }

            let data = JSON.parse(body);
            resolve(data.tree.filter((datum) => datum.type === "blob"));
        });
    }).then((blobs) => {
        let candidates = blobs.filter((blob) => findMatchingCandidate(blob.path));
        return getConfig(candidates, token);
    });
}

function findBlobWithHigherPriority(candidates) {
    if (candidates.length === 0) {
        return undefined;
    }

    if (candidates.length === 1) {
        return candidates[0];
    }

    let result = candidates.find((blob) => blob.path === ".gitlinterrc-service.js");

    if (result) {
        return result;
    }

    result = candidates.find((blob) => blob.path === ".gitlinterrc-service.json");

    if (result) {
        return result;
    }

    result = candidates.find((blob) => blob.path === ".gitlinterrc.js");

    if (result) {
        return result;
    }

    result = candidates.find((blob) => blob.path === ".gitlinterrc.json");

    return result;
}

function getConfig(candidates, token) {
    let defaultConfig = {
        rules: {
            "short-description-length": 2,
            "long-description-length": 2,
            "empty-line": 2
        }
    };

    let blob = findBlobWithHigherPriority(candidates);

    if (!blob) {
        return defaultConfig;
    }

    return new Promise((resolve, reject) => {
        request({
            method: "GET",
            url: blob.url,
            headers: {
                "Authorization": `Bearer ${token}`,
                "User-Agent": "git-linter-service",
                "Accept": "application/vnd.github.machine-man-preview+json"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 200) {
                console.error("Failed to get config file:", body); // eslint-disable-line no-console
                reject(response);
                return;
            }

            let data = JSON.parse(body);
            let content = Buffer.from(data.content, data.encoding).toString();
            resolve(content);
        });
    }).then((content) => {
        return confReader(content, blob.path);
    });
}

function findMatchingCandidate(name) {
    return name.match(/^\.gitlintrc(-service)?\.js(on)?$/);
}

function getComparisonBetween(base, head, url, integration_token) {
    return new Promise((resolve, reject) => {
        request({
            method: "GET",
            url: url.replace("{base}", base.sha).replace("{head}", head.sha),
            headers: {
                "Authorization": `Bearer ${integration_token}`,
                "User-Agent": "git-linter-service",
                "Accept": "application/vnd.github.machine-man-preview+json"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 200) {
                console.error("Failed to get comparison:", body); // eslint-disable-line no-console
                reject(response);
                return;
            }

            let data = JSON.parse(body);
            resolve(data.commits);
        });
    });
}

function getToken({id}) {
    // Private key contents
    let integration_token = jwt.sign({}, integrationPem, {
        algorithm: "RS256",
        expiresIn: "2m",
        issuer: `${integration_id}`
    });

    return new Promise((resolve, reject) => {
        request({
            method: "POST",
            url: `https://api.github.com/installations/${id}/access_tokens`,
            headers: {
                "Authorization": `Bearer ${integration_token}`,
                "User-Agent": "git-linter-service",
                "Accept": "application/vnd.github.machine-man-preview+json"
            }
        }, (error, response, body) => {
            if (!response || response.statusCode !== 201) {
                console.error("Failed to get token:", body); // eslint-disable-line no-console
                reject(response);
                return;
            }

            let data = JSON.parse(body);
            resolve(data.token);
        });
    });
}

module.exports = {installRoutes};
