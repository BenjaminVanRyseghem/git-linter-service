function guiid() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
        let r = Math.random() * 16 | 0;
        let v = c === "x" ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function installRoutes(prefix, router) {
    const jsoncomp = require("jsoncomp");
    const lz = require("lz-string");

    router.get("/info/:data", function(req, res) {
        let {params: {data}} = req;
        // let result = jsoncomp.decompress(JSON.parse(lz.decompress(decodeURI(data))));
        let result = JSON.parse(decodeURI(data));

        let anomalies = result.errors || [];

        let errors = anomalies.filter((anomaly) => anomaly.level === 2);
        errors.forEach((error) => { error.uuid = guiid();});
        let hasErrors = !!errors.length;
        let warnings = anomalies.filter((anomaly) => anomaly.level === 1);
        warnings.forEach((error) => { error.uuid = guiid();});
        let hasWarnings = !!warnings.length;

        res.render("info", {
            title: `Commit info (${result.sha})`,
            message: result.message || "",
            errors: errors,
            warnings: warnings,
            hasErrors: hasErrors,
            hasWarnings: hasWarnings,
            data: JSON.stringify({
                message: result.message,
                errors,
                warnings
            })
        });
    });
}

module.exports = {installRoutes};
